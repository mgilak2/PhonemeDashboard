<?php namespace Dashboard;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Factory as View;
use Phoneme\Contracts\Initializable;
use Phoneme\Contracts\PhonemeComponent;

class DashboardServiceProvider extends ServiceProvider implements Initializable, PhonemeComponent
{
    /**
     * @var View
     */
    private $view;
    /**
     * @var Router
     */
    private $route;

    public function __construct(Application $app, View $view, Router $route)
    {
        parent::__construct($app);
        $this->app = $app;
        $this->view = $view;
        $this->route = $route;
    }

    public function register()
    {
        // nothing to do !
    }

    public function boot()
    {
        $this->viewsBaseFolder();
        $this->defineRoutes();
    }

    private function viewsBaseFolder()
    {
        $this->view
            ->addNamespace('Admin', __DIR__ . "/../../../Themes/Admin/views");
    }

    private function defineRoutes()
    {
        $this->route->get("/dashboard", ['as' => 'dashboard', function () {
            return $this->view->make("Admin::dashboard", ["adminpage" => true]);
        }]);
    }
}