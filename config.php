<?php

return array(
    "name"            => "Dashboard",
    "serviceprovider" => "\\Dashboard\\DashboardServiceProvider",
    "level"           => "init",
    'dir'             =>'Dashboard',
    'theme'           =>'Dashboard',
    'core'            => true,
    'installation'    => true,
    'deactivation'    => true
);