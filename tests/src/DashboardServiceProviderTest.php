<?php

use Dashboard\DashboardServiceProvider;
use Mockery as m;

class DashboardServiceProviderTest extends TestCase
{
    /**
     * @var DashboardServiceProvider
     */
    protected $service;
    protected $mockApp;
    protected $mockView;
    protected $mockRouter;

    public function setUp()
    {
        parent::setUp();

        $this->service = $this->createApplication()
            ->make(DashboardServiceProvider::class);
    }

    public function testBootShouldDefineRoutes()
    {
        $this->service->boot();

        $this->call('GET', '/dashboard');

        $this->assertResponseOk();
    }

    public function testBootShouldDefineViewsBaseFolder()
    {
        $this->service->boot();

        $this->ifCallingARouteRespondBackAsOkMeansOurBaseFolderForViewsAreWokring('GET', '/dashboard');
    }

    private function ifCallingARouteRespondBackAsOkMeansOurBaseFolderForViewsAreWokring($method, $route)
    {
        $this->call($method, $route);

        $this->assertResponseOk();
    }

    public function tearDown()
    {
        parent::tearDown();

        m::close();
    }
}